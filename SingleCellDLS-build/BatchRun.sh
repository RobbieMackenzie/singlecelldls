#!/bin/bash

# This bash script runs the GEANT4 simulation several times, using sed to
# change the value of the photon energy in the macro file.

function Run {
    local label=$2
    local threads=$1
    echo " "
    echo $label
    echo " "

    # Change the gamma energy in the macro file
    sed -i "15 s/\/gps\/ene\/mono .*/\/gps\/ene\/mono $label keV/" SingleCellDLS.mac

    # Run the model with the macro file, dumping output for speed
    ./radioprotection SingleCellDLS.mac > /dev/null

    # Process the data into raw and summary files
    python3 DataProcess.py -j$threads -l$label -f"./" >> CollectedData.csv
}
################################################################################

threads=20
echo "###################### Batch Run Starting ########################"
for n in 53.6 60.48 70.2 79.95 81.08 89.98 99.81 109.77 121.31 129.52 139.19 148.43
    do
    Run $threads $n
    done

